package uj.pesel;

public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Wrong number of arguments provided. Expected <PESEL1> <PESEL2>");
            return;
        }

        PESEL pesel1 = new PESEL(args[0]);
        PESEL pesel2 = new PESEL(args[1]);
        boolean isPesel1Correct = PESEL.check(pesel1.getPesel());
        boolean isPesel2Correct = PESEL.check(pesel2.getPesel());

        if (isPesel1Correct) {
            System.out.println("PESEL1 is correct");
        } else {
            System.out.println("PESEL1 is not correct");
        }

        if (isPesel2Correct) {
            System.out.println("PESEL2 is correct");
        } else {
            System.out.println("PESEL2 is not correct");
        }

        if (isPesel1Correct && isPesel2Correct) {
            if (pesel1.compare(pesel2)) {
                System.out.println("PESELs are the same");
            } else {
                System.out.println("PESELs are different");
            }
        }
    }
}