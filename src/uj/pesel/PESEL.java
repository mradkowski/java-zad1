package uj.pesel;

public class PESEL {
    private final String pesel;
    private static final int[] coefficients = new int[] {
        1, 3, 7, 9
    };

    PESEL(String pesel) {
        this.pesel = pesel;
    }

    boolean compare(PESEL other) {
        return pesel.equals(other.pesel);
    }

    static boolean check(String pesel) {
        if (pesel.length() != 11) {
            return false;
        }
        if (!pesel.matches("\\d+")) {
            return false;
        }

        int sum = 0;
        for (int i = 0; i < pesel.length()-1; i++) {
            int digit = (pesel.charAt(i) - '0') % 10;
            sum += digit * coefficients[i % coefficients.length];
        }

        int checkSum = 10 - (sum % 10);
        int lastDigit = pesel.charAt(pesel.length()-1) - '0';
        return checkSum == lastDigit;
    }

    public String getPesel() {
        return pesel;
    }
}
